export function merge(collection_1: number[], collection_2: number[], collection_3: number[]) {
    const newCollection: number[] = [...collection_1, ...collection_2, ...collection_3]

    let firstArray: number[] = newCollection.slice(0, (newCollection.length / 2))
    let secondArray: number[] = newCollection.slice(newCollection.length / 2)

    let k: number = 0;

    const newArray: number[] = []

    while (newArray.length < newCollection.length) {
        let min = Infinity;

        const firstArrayMin = minimunValue(firstArray)
        const secondArrayMin = minimunValue(secondArray)
        if (firstArrayMin.min < secondArrayMin.min) {
            min = firstArrayMin.min
            firstArray.splice(firstArrayMin.index, 1)
        } else {
            min = secondArrayMin.min
            secondArray.splice(secondArrayMin.index, 1)
        }
        
        if (min < Infinity) {
            newArray[k] = min;
        }

        k++
    }

    return newArray
}

export function minimunValue(array: number[]) {
    let min: number = Infinity;
    let i: number = 0
    let index: number = 0;
    while (i < array.length) {
        if (array[i] < min) {
            min = array[i]
            index = i
        }
        i++
    }
    return { min, index }
}


const collection_1: number[] = [0, 1, 2, 3, 4]
const collection_2: number[] = [4, 3, 2, 1, 0]
const collection_3: number[] = [0, 1, 2, 3, 4]

console.log(merge(collection_1, collection_2, collection_3))