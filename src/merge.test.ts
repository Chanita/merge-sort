import { merge, minimunValue } from './merge';

describe('merge function', () => {
    test('merges and sorts three collections where collection_1, collection_3 already sorted from min(0) to max collection_2 already sorted from max to min(0)', () => {
        const collection_1: number[] = [0, 1, 2, 3, 4]
        const collection_2: number[] = [4, 3, 2, 1, 0]
        const collection_3: number[] = [0, 1, 2, 3, 4]

        const result = merge(collection_1, collection_2, collection_3);
        const expected = [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4]

        expect(result).toEqual(expected);
    });

    test('merges and sorts collections where collection_1, collection_3 and collection_2 unsorted', () => {
        const collection_1: number[] = [23, 8, 9, 4, 45];
        const collection_2: number[] = [56, 2, 12, 6, 5];
        const collection_3: number[] = [2, 45, 10, 3, 21];

        const result = merge(collection_1, collection_2, collection_3);
        const expected = [2, 2, 3, 4, 5, 6, 8, 9, 10, 12, 21, 23, 45, 45, 56]

        expect(result).toEqual(expected);
    });
});

describe('minimunValue function', () => {
    test('find minimum value and index in array', () => {
        const array = [3, 1, 4, 1, 5, 9];
        const result = minimunValue(array);
        const expected = { min: 1, index: 1 }
        expect(result).toEqual(expected);
    });

    test('return Infinity and index 0 for empty array', () => {
        const array: number[] = [];
        const result = minimunValue(array);
        const expected = { min: Infinity, index: 0 }
        expect(result).toEqual(expected);
    });
});
