## Installation

### Prerequisites
Make sure you have [Node.js](https://nodejs.org/en/download/package-manager) installed on your machine.

### Steps

1. **Clone the repository:**
   ```sh
   $ git clone https://gitlab.com/Chanita/merge-sort.git
   $ cd merge-sort

2. **Install dependencies**
    ```sh
    $ npm install

## Usage

```bash
# Updating npm
$ npm install -g npm@latest

#Checking Versions
$ node -v
$ npm -v

#Running the Development Server#
$ npm run dev

#Running Tests#
$ npm run test